﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(tinker.Startup))]
namespace tinker
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
