﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using tinker.Models.ajax;

namespace tinker.Models
{
    public class TinkerDb : DbContext
    {
        public TinkerDb()
            : base("name=DefaultConnection")
        {

        }

        public DbSet<Person> People { get; set; }

    }
}