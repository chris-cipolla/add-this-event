﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using tinker.Models;
using tinker.Models.ajax;

namespace tinker.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult EventClicked(object data)
        {
            return Json(new { name = "test", date = DateTime.Now });
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public JsonResult AddPerson(Person aperson)
        {
            if (aperson == null)
            {
                return Json("fail");
            }

            var db = new TinkerDb();
            db.People.Add(aperson);
            db.SaveChanges();

            return Json(aperson);
        }
    }
}